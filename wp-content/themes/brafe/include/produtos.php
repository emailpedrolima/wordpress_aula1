<section class = "section3">
        <nav id = "cidades">
        <div id ="Paulista">
            <img src="<?php the_field('icone_cidade_1')?>" id = "circle">
            <h2><?php the_field('primeira_cidade') ?></h2>
            <p id="section3"><?php the_field('texto_condicoes_climaticas') ?></p>
        </div>
        <div id ="Carioca">
            <img src="<?php the_field('icone_cidade_2')?>" id = "circle">
            <h2><?php the_field('segunda_cidade') ?></h2>
            <p id="section3"><?php the_field('texto_condicoes_climaticas') ?></p>
        </div>
        <div id ="Mineiro">
            <img src="<?php the_field('icone_cidade_3')?>" id = "circle">
            <h2><?php the_field('terceira_cidade') ?></h2>
            <p id="section3"><?php the_field('texto_condicoes_climaticas') ?></p> 
        </div>
        </nav>
        <?php if(!is_page('produtos')) {?> 
        
        <input type="button" id="saibaMAis" class = "animação1" value = "SAIBA MAIS"></input>
        <? } ?>
    </section>
