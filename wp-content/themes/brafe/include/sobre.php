<section class = "section2">
        <h2><?php the_field('titulo-section2') ?></h2>
        <nav id ="cafe">
            <div id = "amor">
                <figure>
                    <img src="<?php the_field('section_2_foto_amor') ?> ">
                    <figcaption id ="a">amor</figcaption>
                </figure>
            
            </div>
            <div id = "perfeicao">
                <figure>
                    <img src="<?php the_field('section_2_foto_perfeicao')?>">
                    <figcaption id ="p">perfeição</figcaption>
                 </figure>
          
            </div>
        </nav> 
        <p id = "section2">
            <?php the_field('texto-section2') ?>
        </p>
    </section>