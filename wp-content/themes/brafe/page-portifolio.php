<?php
// Template Name: Portifolio
?>


<?php get_header();


$paged = (get_query_var('page',1)); 




$news = new WP_Query(
    array(
        'posts_per_page'         => 2,
        'paged'                 => $paged,
        'post_type'             => "noticia",
        'post_status'           => 'publish',
        'suppress_filters'      => true,
        'orderby'               => 'post_date',
        'order'                 => 'ASC'
    )
);

?>
<h1 class = "outrasPag" ><?php the_field('titulo_portifolio') ?></h1>

<div id = "news-container" class = "container">

<?php if ($news -> have_posts()):
        
    while($news -> have_posts()):
        $news -> the_post(); ?>

            <div class ="news card">
                <h2><?php the_title(); ?> </h2>
                <?php the_content(); ?>
            </div>
        
    <?php endwhile;
    else:?>
        <p>Não temos notícias</p>

    <?php endif; ?>
</div>

<div id ='news-pgnt'>
    

    <?php $big = 999999999;
        echo paginate_links( array (
            'base' => str_replace($big,'%#%',get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(2,get_query_var('paged')),
            'prev_text'       => __('Anterior'),
            'next_text'       => __('Próximo'),
            'total ' => $news->max_num_pages)
        );

        ?>
</div> <!--- Por algum motivo , não funcionou --> 


<?php wp_reset_postdata();?>




<?php get_footer()?>