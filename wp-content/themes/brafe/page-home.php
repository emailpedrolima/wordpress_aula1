<?php get_header()?>


<?php
// Template Name: Home Page
?>


<body class = "container">


    
    <section class = "section1">
        <div id = "titulo">
            <h1 id="titulo"><?php the_field('titulo_chamativo') ?></h1>
            <h2>—</h2>
            <p id = "section1"><?php the_field('sub-titulo_chamativo') ?></p>
        </div>
    </section>

     <!--chamar Fragmento de sobre -->

    <?php include get_template_directory().'/include/sobre.php' ; ?>
    

    <!--chamar Fragmento de produtos -->

    <?php include get_template_directory().'/include/produtos.php' ; ?>

    <section class = "section4">

            <div id = "Botafogo">
                <figure>
                    <img src="<?php the_field('foto_filial_1')?>">
                </figure>
                <nav id = "Botafogo">
                <h2><?php the_field('filial_1') ?></h2>
                <p id="section4"><?php the_field('endereco_filial_1') ?></p>
                <input type ="button" id ="verMapa"  class = "animação1" value ="VER MAPA"></input>
                </nav>
            </div>
            <div id = "Iguatemi">
                <figure>
                    <img src="<?php the_field('foto_filial_2')?>">
                    
                 </figure>
                <nav id = "Iguatemi">
                <h2><?php the_field('filial_2') ?></h2>
                <p id="section4"><?php the_field('endereco_filial_2') ?></p>
                <input type ="button" id ="verMapa"  class = "animação1" value ="VER MAPA"></input>
                </nav>
            </div>
            <div id = "Mineirao">
                <figure>
                    <img src="<?php the_field('foto_filial_3')?>">
                    
                 </figure>
                 <nav id = "Mineirao">
                <h2><?php the_field('filial_3') ?></h2>
                <p id="section4"><?php the_field('endereco_filial_3') ?></p>
                <input type ="button" id ="verMapa"  class = "animação1" value ="VER MAPA"></input>
                </nav>
            </div>


    </section>
    <section class = "section5">

        <div id = "newsletter">
            <h2>Assine Nossa Newsletter</h2>
            <p id="section5">promoções e ventos mensais</p>
        </div>
        
        <div id = "email">
            
            <input type = "text" id = "email" placeholder="  Digite seu e-mail"></input>
            <button id = "enviar">ENVIAR</button>

        </div>

    </section>

    <?php get_footer() ?>