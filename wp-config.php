<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h7xw9EWBRClOJfPVAD4jt6r9L4sHzS4JvhCYpcr8WLYDg0ggRZCh+89iRGULftXpQfxKeq4g0jCfowok5rUqsg==');
define('SECURE_AUTH_KEY',  'jOhaT1WZO5Z2ZS6NBmR5B7bKYvj4VQrNq4IM4kqBlU0la57FkIxZlVSJRDWTPwCrggPq0carAeSU2GInH7MYgQ==');
define('LOGGED_IN_KEY',    'PCn/w2bDECTXZSGgiz+UXEOlbPQu+B245sCUkx0hq40Dv/Wua7dITUYDEdqf1diEg5NGcy25pdp0ljwuk4m2LA==');
define('NONCE_KEY',        '8X/StAkK0E0BFqgWSHN3ruj70D37k33aIemGOrhzhGX0GQvlGTphvRLcJad/kHMNcui+Zr5RWXZzL8KlamCYKg==');
define('AUTH_SALT',        'MhaDZ4x9Tj7vq25io2h15C0VGeu6FlTrXhsZGm/cHrN0GJ/vF6iK45KILAriQG2GVdcI63n55edeLP83g6pfWQ==');
define('SECURE_AUTH_SALT', '4lhDQjXdDuqwKm/SSigZVACSxi+ddpPcT6HYEhRKCMwrDQUueC/qZe0PSL+fYSgcaQmcnzRTWFPiLsBssa35Aw==');
define('LOGGED_IN_SALT',   'k35a0DfODRkXjyaFRu6kRKTI4KkFE3PUyaIyvtzg3V2UBvN87Gl8z8XG7oHOE5N/zG8/wX1iWivYjfIr4PNc2g==');
define('NONCE_SALT',       'ugsCl9rtOiFy+M+te628SLAPxH/C3JXW1FAZg5ditfxHFvDbFmqwhhkrznpNoSWHcJ52vUpyVXEQK86IjSO7Zg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
